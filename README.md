# lecture-materials

This repository contains lecture materials for the Generic Software Skills course at UHH.

[👉 This way to the most recent version of the notes](https://generic-software-skills.gitlab-pages.dkrz.de/lecture-materials/)

## Updating lecture material.

This project uses [Quarto](https://quarto.org), please install it according to their [getting started guide](https://quarto.org/docs/get-started/).

Once installed, you can clone this git repository and run a local preview server using:

```
quarto preview --no-navigate
```

(if you omit `--no-navigate`, the preview server would automatically navigate to the most recent modified file, which in our case is likely the automatically generated summary file).

Once the preview server is running, please open it in your browser (if it didn't do this automatically) and go on modifying the source files.

### Adding new lecture slides

Lecture slides have to be added as a Quarto markdown file to the `lectures` folder.
Specifically, each lecture has to be in a separate subfolder and should be named e.g.: `lectures/example-lecture/slides.qmd`.
Please make sure, that the name of the folder identifies the lecture briefly and does **not** include other information (e.g. the ordering number of the lecture).
You may also check the current `_quarto.yml` for suggestions.

Each slide needs to have a `title` and `author` set in it's [Front Matter](https://quarto.org/docs/authoring/front-matter.html).
Otherwise the file is a Markdown file with special features from [Quarto](https://quarto.org/docs/guide/).

In order to make the new slide deck visible on the lecture website, you'll **also** have to add (or uncomment) the filename of the `website.sidebar.contents` entry in the `_quarto.yml` file.
Please add the file in order of the planned lecture schedule.
