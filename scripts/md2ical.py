#!/usr/bin/env python3
import argparse
import os
import pathlib

import markdown2
from bs4 import BeautifulSoup
from icalendar import Calendar, Event
from datetime import datetime, timezone


def extract_lecture_dates(markdown_file, output_file):
    with open(markdown_file, "r") as file:
        if markdown_file.suffix == ".html":
            html_content = file.read()
        else:
            html_content = markdown2.markdown(file.read(), extras=["tables"])

    cal = Calendar()
    cal.add("prodid", "-//md2ical//Generic Software Skills Lectures//EN")
    cal.add("version", "2.0")
    cal.add("x-wr-calname", "GSS Lectures")
    cal.add("REFRESH-INTERVAL;VALUE=DURATION", "P1D")

    soup = BeautifulSoup(html_content, "html.parser")
    for table_entry in soup.findAll("tr")[1:]:
        date, title, lecturers = [e.contents[0] for e in table_entry.findAll("td")]

        if lecturers == "-" or table_entry.find(class_="inactive"):
            # Table entries without lecturer are considered to be breaks/holidays.
            continue

        # Define lecture start/end. Needs to be adapated every semester!
        dt = datetime.strptime(date, "%Y-%m-%d")
        lecture_start = datetime(dt.year, dt.month, dt.day, 13)
        lecture_end = datetime(dt.year, dt.month, dt.day, 15)

        event = Event()
        event.add("summary", f"{title} ({lecturers})")
        event.add("uid", f"{title}+{lecturers}".replace(" ", "+").replace(",", ""))
        event.add("dtstamp", datetime.now(timezone.utc))
        event.add(
            "dtstart;tzid=/europe/berlin", lecture_start.strftime("%Y%m%dT%H%M%S")
        )
        event.add("dtend;tzid=/europe/berlin", lecture_end.strftime("%Y%m%dT%H%M%S"))
        event.add("location", "Geomatikum, Room 1536a")
        cal.add_component(event)

    with open(output_file, "wb") as f:
        f.write(cal.to_ical())


def main():
    parser = argparse.ArgumentParser(
        prog="md2ical",
        description="Convert a Markdown calendar table to an iCal calendar.",
    )
    parser.add_argument(
        "-f",
        "--filename",
        default=pathlib.Path(os.environ.get("QUARTO_PROJECT_OUTPUT_DIR", ""))
        / "index.html",
    )
    parser.add_argument(
        "-o",
        "--output",
        default=pathlib.Path(os.environ.get("QUARTO_PROJECT_OUTPUT_DIR", ""))
        / "lectures.ics",
    )

    args = parser.parse_args()

    extract_lecture_dates(args.filename, args.output)


if __name__ == "__main__":
    main()
