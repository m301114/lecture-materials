import logging
from itertools import chain
import yaml
import markdown2
from bs4 import BeautifulSoup

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

# But you can use your content.


def parse_file(filename):
    logging.debug(f"opening {filename}")
    with open(filename, "r") as f:
        content = f.read()
        html_content = markdown2.markdown(
            content, extras=["metadata", "tables", "html-classes"]
        )

    yield 0, html_content.metadata["title"].strip('"')

    soup = BeautifulSoup(html_content, "html.parser")
    for heading in soup.find_all(["h1", "h2"]):
        level = int(heading.name[1])
        text = heading.text.split("{")[0].strip()
        yield level, text


def format_entry(level, text):
    prefix = ["\n# ", "\n## ", "* ", "  * "]
    return prefix[level] + text + "\n"


summary_header = """---
title: "Lecture Overview"
---
"""


def generate_summary():
    project_config = yaml.safe_load(open("_quarto.yml"))

    files = [
        fn
        for s in project_config["website"]["sidebar"]["contents"]
        if s.get("section") == "Lectures"
        for fn in s["contents"] or []
    ]

    all_entries = list(chain.from_iterable(map(parse_file, files)))

    with open("summary.qmd", "w") as outfile:
        outfile.write(summary_header)
        for level, text in all_entries:
            outfile.write(format_entry(level, text))


def main():
    generate_summary()


if __name__ == "__main__":
    exit(main())
